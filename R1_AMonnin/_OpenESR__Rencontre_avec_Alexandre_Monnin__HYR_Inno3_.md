---
robots: index, follow
lang: fr
tags: conf, recherche, openESR
---

# [OpenESR] Rencontre avec Alexandre Monnin (HYR/Inno3)

> [name=Auteur.e.s :Maya Anderson, Vincent Bachelet, Célya Gruson-Danie]
> [name=Relecteur : Alexandre Monnin]
> [time=26 february 2020] [color=red]

Contenu sous Licence ![CC-BY-SA 4.0](https://licensebuttons.net/l/by-sa/4.0/80x15.png)

## Première rencontre OpenESR avec [Alexandre Monnin](https://www.esc-clermont.fr/professeurs/alexandre-monnin-professeur-en-strategie-digitale/) 

:::info
**Rencontres OpenESR** : 
- **recevoir des chercheur.e.s ou expert.e.s** spécialisé.e.s sur les communs (conceptions, axes de recherche, courants de pensée au sein même du mouvement, rôle d'autres disciplines dans l'évolution des concepts) et/ou des personnes qui s'inscrivent dans des démarches de communs sans s'en revendiquer ; 
- **porter un regard réflexif** sur ce que ces projets/développements de concepts peuvent apporter au système de l'enseignement supérieur et de la recherche (ESR) et les relations sciences/sociétés.
:::

Invité au 137 par le cabinet Inno³ et l'association HackYourResearch (évolution de HackYourPHD), le philosophe du Web [Alexandre Monnin](https://twitter.com/aamonnz) est venu nous rencontrer le 26 février 2020 pour parler de ses travaux de recherche sur les communs et de la manière dont ses réflexions peuvent inspirer l'Enseignement Supérieur et la Recherche (ESR). Cet article résume son intervention et s'accompagne d'un entretien audio qui apportera des informations complémentaires.

<iframe src="https://archive.org/embed/openesr-alexandre-monnin" width="500" height="30" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

## Présentation

:::warning
ITW [min. 0:00 - 2:45]
:::

**Alexandre Monnin** est philosophe, enseignant-chercheur en stratégie numérique et design à l'ESC Clermont, mais aussi Directeur scientifique d'[Origens Media Lab](https://origensmedialab.org/) et ancien Président de l’association Adrastia. Docteur en philosophie de l’Université Paris 1 Panthéon-Sorbonne, il a consacré sa thèse à la philosophie du Web. Il est aussi membre du réseau d’experts de la mission Etalab, du GDS Ecoinfo (CNRS) et du GDR CIS (CNRS). Il a également été chercheur  chez Inria, architecte de la plateforme numérique de Lafayette Anticipations et responsable recherche Web à l’IRI du Centre Pompidou. Il a aussi été consultant pour l'Institut International de Planification de l'Éducation, un organisme de l’UNESCO et collabore avec le [Shift project](https://theshiftproject.org/wp-content/uploads/2018/11/Rapport-final-v8-WEB.pdf).


## Contexte et évolution de son parcours


:::warning
ITW [min. 2:45 - 5:00]
:::

**Janvier 2014** : 
 - Partage en ligne de la thèse d'A. Monnin sur le site [Philoweb](http://web-and-philosophy.org/) en format ouvert et réutilisable. 
 - Rencontre avec l'association HackYourPHD: [lire l'interview](http://hackyourphd.org/2014/01/interview-dalexandre-monnin-une-these-augmentee-avec-philoweb-org/)

**2016/2017** : 
 - Entretien sur un projet de coopérative

**Aujourd'hui** : 
 - Retour sur l'évolution des outils et du partage [CGD : quels outils et partage ?]
 - Questionner les pratiques de recherche actuelles et leurs évolution. 
 - Regard réflexif sur la recherche

## Documentation et communs 

:::warning
ITW [min. 5:00 - 10:10]
:::

Aujourd'hui, pour A. Monnin un constat peut être fait : dans l'enseignement supérieur et la recherche si l'on documentait, on documenterait moins la recherche que les procédures administratives qui se sont imposés aux chercheurs. Une pratique de documentation accompagnée par la création d'outils adaptés peut néanmoins impulser la création d'un commun numérique de la documentation de la recherche.



À ce titre, A. Monnin a été l'architecte d'une plateforme de documentation pour [Lafayette Anticipations, la Fondation d'entreprise Galeries Lafayette](https://www.lafayetteanticipations.com/fr) créée dans l'optique de comprendre et faire comprendre le fonctionnement de l'art contemporain, qui ne répond pas à la définition classique du beau ou de l'oeuvre. 

Dans ce contexte, l'objectif de la documentation est de montrer qu'il y a des opérations de valuation faites tout au long de la production d'une oeuvre ou d'un projet - A. Monnin préfère cependant parler de trajet, terme qui permet mieux de prendre en compte l'inattendu, ce que l'on ne projette pas a priori. 

:::success
La plateforme de documentation de Lafayette Anticipations repose sur le projet global Re-Source, qui est construit sur des solutions *open source* dévéloppées par la coopérative Mnmotix notamment [Weever](https://github.com/Mnemotix/weever-documentation) et [Koncept](https://github.com/Mnemotix/koncept-documentation)
:::

Testé en contexte pédagogique à la [Villa Arson](https://www.villa-arson.org/) à Nice dans le cadre d'un projet financé par le Ministère de la Culture et présenté au cours de la journée d'études ["Documenter la production artistique : données, outils, usages"](https://www.villa-arson.org/2018/06/documenter-la-production-artistique-donnees-outils-usages-autour-des-plateformes-resource-et-artefactory/), cet outil permet notamment :
- la création d'une **archive** de la vie de la fondation ;
- l'implémentation de **timelines** avec des ressources dans lesquelles on peut associer des évènements ;
- l'inclusion de ressources avec des **tags** pour mieux documenter, indexer puis retrouver ;
- la possibilité de générer une **archive sémantique** qui peut être réexprimée de plusieurs façons, par exemple sur un site web.


## Transformations du paysage de la recherche

:::warning
ITW [10:11 - 14:18]
:::

A. Monnin est revenu sur les développements qui bouleversent actuellement le milieu de la recherche, et a notamment expliqué les caractéristiques du [programme IDEX](https://www.enseignementsup-recherche.gouv.fr/cid127921/www.enseignementsup-recherche.gouv.fr/cid127921/www.enseignementsup-recherche.gouv.fr/cid127921/session-du-jury-initiatives-d-excellence-idex.html). Depuis 2010, ces programmes transforment le paysage de la recherche en France selon une logique internationale d'évaluation et de privatisation. Dans les faits, les universités et les grandes écoles se sont regroupées pour former des COMUE (COMmunauté d'Universités et Établissements), leur permettant de candidater à des financements.

:::info
**IDEX (Initiative D'EXcellence)**: Selon A. Monnin, elles sont l'illustration d'une autonomie pour les institutions au sens de  "il faut trouver soi-même son propre argent," contrairement au sens étymologique du terme "autonomie" comme faculté d'agir librement en se donnant ses propres lois.
:::

A. Monnin déplore que ces changements désormais généralisés entrainent un désinvestissement de l'État et une managérialisation des institutions d'enseignement, de recherche et d'innovation, où une minorité d'entre elles, "choisies", peuvent accéder aux financements pour effectuer des recherches et rester innovantes alors qu'une majorité  se consacreront à l'avenir à l'enseignement et n'auront plus vocation à faire de la recherche (modèle similaire aux community colleges au USA).

On constate qu'aujourd'hui le financement de la recherche se fait majoritairement par appel à projets. L'évaluation du projet ne se fait d'ailleurs qu'au stade du financement et il n'y a que rarement des évaluations des rendus et livrables. Il existe une prétention que ce qui est fait a un impact, un phénomène qui s'accompagne d'un effet d'affichage très fort. Cependant, le transfert de valeur pour la société et la pérennisation des livrables  restent très faibles.

Ainsi, les laboratoires de Sciences Humaines et Sociales (SHS) ne peuvent prétendre à de gros financement et ce format leur laisse de moins en moins la possibilité d'effectuer les enquêtes sur lesquelles ils fondent leurs recherches. Avec ce mode de fonctionnement, les institutions sont souvent inadaptées aux enquêtes de terrain pour des sujets contemporains ou des analyses de phénomènes sociaux à l'oeuvre. Le problème est aggravé par l'absence de financements destiné au fonctionnement (les fonds propres des laboratoires). 

> Au niveau des institutions, on ne répond pas aux besoins exprimés par les chercheurs sur le terrain, mais on en appelle plutôt à un darwinisme social dans la recherche (explicitement revendiqué par le PDG du CNRS), ce qui pour A. Monnin nécessite de repenser et *redesigner* le modèle économique de l'ESR.

## Quelles alternatives ?

:::warning
ITW [14:19 - 17:00]
:::

En réponse au démantèlement, A. Monnnin précise qu'il s'agirait de mettre en place des modèles plus pérennes, qui transforment l'ESR de l'intérieur. Pour cela, on peut penser au modèle des coopératives de recherche, développées pour répondre à un projet en pensant sa pérennité. Dans cette optique, les livrables fournis pourraient pensés comme des communs afin d'être réutilisés par d'autres projets et d'autres collectifs. Cela favoriserait la création de consortiums qui offriraient la possibilité d'intéger des chercheurs restés en marge de la recherche et de leur proposer un CDI au sein d'une coopérative (plutôt que des CDI de projets, moins pérennes). A. Monnin reconnaît néanmoins une ambiguité puisque le contrat d'un chercheur resterait dépendant du financement de la coopérative - mais pas du projet comme c'est le cas actuellement.

:::info
Un exemple de projet ANR pérennisé : **[Mnemotix](https://www.mnemotix.com/)**, la smartup coopérative ancrée dans l’innovation et la recherche.
:::

Ce changement de modèle favoriserait à la fois une économie de la demande et non de  l'offre, tout en redonnant l'initiative aux laboratoires et non pas à l'Agence Nationale de la Recherche (ANR) qui répercute les grands programmes industriels et politiques (du côté de l'offre). Cela augmenterait également la "compostabilité" de la recherche dans la mesure où les livrables produits seraient ouverts et réutilisables. On échappe ainsi à la problématique des résultats qui disparaissent lorsque le financement d'un projet touche à sa fin. Sur le modèle des stratup dont les projets ANR se rapproche paradoxalement (durée de vie très courte - 3 ans, livrables incertains, etc.)

:::info
**[Oxamyne](https://www.oxamyne.org/)** en association avec le laboratoire citoyen **[La MYNE - Manufacture des Idées et des Nouvelles Expérimentations](https://www.lamyne.org/)** constitue un exemple de coopérative de recherche aux caractéristiques particulières:
- Coopérative d'activités et d'emploi (CAE) 
- en dehors du monde académique 
- avec des chercheurs salariés et des doctorant.e.s
:::

La loi de programmation pluri-annuelle de la recherche (LPPR) en cours de discussion promouvrait une logique d'*ubérisation* de la recherche et l'émergence de nouveaux laboratoires hors des universités. Il faut ainsi être vigilant à ce que la création de structures coopératives indépendantes motivées par la volonté de sortir les chercheurs de la précarité ne participe pas *in fine* à un mouvement global de précarisation.

## Projet "closing worlds" 

:::warning
ITW [17:01 - 18:47]
:::

A. Monnin, par ses études sur la philosophie de l'architecture du web, s'est particulièrement questionné sur l'avenir du numérique et la possible fin du numérique. Il s'appuie sur les concepts développés par le sociologue Bruno Latour (anthropologie de laboratoire, anthropocène)

:::info 
**Anthropocène** : époque géologique marquée par les conséquences de l'activité humaine et industrielle avec un impact sur l'habitabilité de la terre. 
:::

A partir de 2013, A. Monnin se rapproche du laboratoire Origens Média Lab qui travaille sur l'antropocène. C'est dans ce cadre que sa pensée sur les communs a évolué, en revenant sur l'utopie que constitue pour beaucoup le numérique. 

:::info
**Origens Média Lab** : [laboratoire de recherches](https://origensmedialab.org/) conçu comme un tiers-lieu interdisciplinaire en sciences humaines et sociales, qui se propose d’enquêter sur ce qui se joue derrière la crise écologique que nous traversons. 
:::

Dans cette pensée des communs, la place des infrastructures dont nous allons hériter est essentielle. Ces dernières sont considérées comme des communs négatifs tout comme des sols pollués, des montagnes sans neige, etc.). Il s'agit ainsi de questionner leurs gestions futures et non pas seulement se concentrer sur les ressources positives (des champs, des rivières poissonneuses, etc.). À ce titre, d'un point de vue écologique, le développement de l'Internet des objets (IoT) est vecteur d'un grand nombre de communs négatifs, qui n'est pas ou peu abordé aujourd'hui.

Dans l'optique du renouvellement des communs, plusieurs questions se posent:

- Comment programmer l'obsolescence? 
- Comment ne pas faire advenir des innovations dangereuses d'un point de vue environnemental ? 
- Comment l'anthropocène transforme-t-il les organisations ? 
- Comment implémenter une ingénierie de la désinnovation, du désinvestissement ? 
- Comment prendre en compte des non-humains dans le droit ? 
    - En Bolivie, la divinité Pacha Mama est inscrite dans la Constitution
    - En Equateur, les non-humains ont des droits
- Comment faire émerger de nouveaux réglements ?

## La recherche : défis actuels

:::warning
ITW [18:48 - fin]
:::

Questionné sur ce qu'il reste à faire pour impulser des changements  dans le milieu de la recherche, A. Monnin évoque les difficultés  que beaucoup éprouvent au sein de leur institution. Dans ce contexte, il peut être utile de tisser des alliances entre les personnes qui appartiennent aux institutions et celles en dehors, à la marge. Il cite notamment les travaux de [F. Chateauraynaud](https://fr.wikipedia.org/wiki/Francis_Chateauraynaud) sur le besoin de cultiver la relation entre les marges et la recherche académique. 

Selon A. Monnin, la meilleure manière d'effectuer la transition vers un nouveau modèle viendra d'une lutte au sein même de l'institution. Comment retrouver de la collégialité, faire émerger des formes nouvelles de collaboration ?

C'est pourquoi de nouvelles formes de financement de la recherche se développent, et d'autres restent à imaginer. Par exemple, pourquoi ne pas implémenter en recherche le modèle de la demande et de la commande que l'on trouve dans le domaine artistique ? 

Cela offrirait la possibilité à des collectifs de s'organiser et de faire de la recherche avec des chercheur.e.s qui sont considérés comme des co-enquêteur.ice.s. 

:::info
C'est la dynamique développée dans le cadre du projet [**CooPair**](https://origensmedialab.org/origens-media-lab/le-protocole-coopair/) avec l'association [**Eleveurs Autrement**](http://www.eleveurs-autrement.fr), qui organise une réflexion des éleveurs sur leur subsistance et permet la production de nouveaux savoirs à cet effet.
:::

Reste à définir comment généraliser cette pratique, notamment en formalisant un protocole de pratiques à destination des institutions souhaitant travailler avec des chercheur.e.s.

:::success
**Aide à penser et à nourrir pour une réflexion critique en recherche**  : 

- Penser non plus **"projets"** mais **"trajets"**.
- Faire la distinction entre **"institutions"** et **"organisations"**. 
    - Il s'agit de considérer que les organisations ne sont pas éternelle et immuables mais liées historiquement à l'émergence du management et à une certaine organisation du travail et visée de l'afficacité qui les distingue des institutions (ce que l'on a tendance à oublier car ces dernières tendent à devenir des organisations).« Organiser, c’est faire société, à chaque instant, au moyen d’un agencement rationnel d’artefacts, d’individus et de normes. 
    - Une organisation n’est pas une institution, au sens où elle est sans fondements constitutifs ni permanence. Elle se recrée continuellement et elle est vouée à disparaître. Elle ne fait sens que par rapport à l’horizon en devenir de ses composantes et de ses finalités, et non en référence à un passé fondateur ou à un ordre transcendant, immémorial et immuable. Le management n’assure pas la perpétuation d’un groupe en organisant ses activités productives ; il assure la perpétuation d’activités productives en organisant les groupes chargés de les accomplir. En d’autres termes, le manager produit rationnellement non pas des biens ou des services, mais des groupes arrangeables, ontrôlables et efficaces." (Thibault Le Texier, Le maniement des hommes). Emmanuel Bonnet parle de "monde organisé" pour décrire ce phénomène, Baptiste Rappin de "mouvement panorganisationnel", Yves-Marie Abraham (à partir d'Andru Solé), d'"entreprise-monde". Voir par exemple la présentation d'Alexandre Monnin "[hériter et prendre soin d'un monde entrain de se défaire](https://chaire-philo.fr/heriter-et-prendre-soin-dun-monde-en-train-de-se-defaire/)""
- Transformer l'ESR sur **un mode coopératif** et non pas ajouter des coopératives périphériques sans modifier le système.
- Faire attention à la notion **d'autonomie** qui aujourd'hui s'éloigne de la dimension étymologie initiale (ie. **"faculté d'agir librement"**) pour signifier dans les discours des moyens pour subvenir à  ses propres financements
- Questionner les interactions entre les marges et les personnes à l'intérieur des systèmes.
:::

--- 
## Pour en savoir plus

- Bonnet E., Landivar D., Monnin A., 2019. Collectif de 24 chercheurs. Les grandes écoles doivent former à la « redirection écologique », Le Monde.fr, 04 octobre.

- Bonnet E., Landivar D., Marmorat S., Monnin A., Nivet B., Ramillien E., 2018. Il est temps de changer en profondeur la manière de penser le périmètre d’action des entreprises, LeMonde.fr, 5 mars 

