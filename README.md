# Rencontres OpenESR

## Objectifs

Nouveau temps d’échange initié par [inno³](https://inno3.fr/) en collaboration avec l’association HackYourResearch/[HackYourPhD](https://hackyourphd.org/), les rencontres OpenESR donnent la parole à des chercheur.e.s et expert.e.s spécialisé.e.s sur les problématiques des communs et de l'*open* tout autant qu'à des personnes qui s'inscrivent dans ces démarches sans forcément s'en revendiquer. L'objectif est de porter un regard réflexif sur ce que ces projets/développements de concepts et de modèles peuvent inspirer en termes de transformation organisationnelle au sein de l'Enseignement Supérieur et de la Recherche (ESR) et des relations sciences/sociétés.

Dans ce répertoire vous trouverez :
- les sources de la [page OpenESR](https://openesr.frama.io/rencontres/)
- les sources des interviews avec chaque intervenant.e.


## Contenu Projet rencontres

- [R1 : Alexandre Monnin le 26 février 2020 au 137](./R1_AMonnin)

- [R2 : Christsophe Masutti le 26 mai 2020 en ligne](./R2_CMasutti)

- [R3 : Florence Piron le 29 juin 2020 en ligne](./R3_FPiron)
