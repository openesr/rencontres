# Charte Éthique pour la confiance dans les démarches de participation ayant recours au numérique


**Licence :** CC By-SA 4.0 https://creativecommons.org/licenses/by-sa/4.0/
Charte rédigée par [OpenLaw](https://charteethiqueparticipation.frama.io/charte-ethique/)


:::info
:mega: Cette Charte a pour objectif de **donner confiance** à l'ensemble des parties prenantes qui contribuent à une **démarche participative** ayant recours au numérique. 

Générique, elle est mobilisable dans **tout type de démarche participative, ponctuelle ou permanente** (budget participatif, consultation, boîte à idées, etc.).
:::

Source d'émancipation, le recours au numérique constitue également une  source de défiance et implique de **mettre en œuvre des dispositifs éthiques et respectueux des enjeux démocratiques.**

En utilisant cette Charte, le Responsable et les Participants s'engagent ainsi à **faire un usage loyal, sincère et proportionné des technologies pour atteindre leur objectif commun**, et plus particulièrement à :

* Mettre en œuvre une Démarche collaborative entre les parties prenantes **[Collaboration]** ; 

* Garantir le meilleur degré  de participation des publics **[Participation]** ;

* Être transparents  autant d'un point de vue technique que méthodologique **[Transparence]** ; 

* Exprimer des engagements réalistes et donner les moyens de leur suivi **[Engagement]**.

## [Section 0] Préambule

### 0. Définitions

* **Démarche participative :** tout type de démarche par laquelle un Responsable sollicite la participation de contributeurs afin d'éclairer une décision ou de co-construire un objet. La Démarche peut mobiliser plusieurs Dispositifs numériques de participation. 

* **Gouvernance :** désigne l'ensemble des mesures, des règles, des organes de décision, d'information et de surveillance qui permettent de fournir l'orientation stratégique et d'assurer le bon fonctionnement de la Démarche. 

* **Responsable :** organisation ou ensemble de personnes qui initie, organise, outille et anime la Démarche. Lorsque le Responsable fait appel à des intervenants techniques, il s'engage à ce que leurs contributions soient effectuées dans le respect de la Charte. 

* **Participant :** personne physique ou morale participant à la démarche pour son compte ou pour le compte d'un tiers.

* **Participation** : ensemble de mesures de co-construction favorisant la confiance dans les rapports entre les acteurs et permettant de parvenir à un résultat partagé et commun à l'ensemble des Participants. 

* **Parties** : ensemble des acteurs impliqués dans la Démarche. 

* **Dispositifs numériques de participation [DNP] :** ensemble de l'infrastructure  numérique mise en œuvre pour la Démarche. Les Dispositifs peuvent être de formes variées (application, plateforme web, site contributif) et inclure différentes fonctionnalités (dépôt d'idées, commentaires, votes, cartographie, etc.). 



### 1. Parties prenantes
    
Les Démarches participatives reposent sur une action collectivement assumée par tous les acteurs impliqués :

* Le(s) Responsable(s) qui initient la Démarche ;

* Les intervenants techniques (assurant notamment la mise à disposition de Dispositifs numériques et d'éventuels accompagnements techniques et méthodologiques) ; 

* Les Participants qui contribuent à la Démarche ;

* Toute éventuelle personne morale ou physique associée à la démarche.


Cette Charte définit ainsi des engagements à la charge de tous. Des responsabilités spécifiques sont associées à chacun de ces différents rôles, étant entendu qu'un même acteur peut cumuler les rôles et les responsabilités. 
    
### 2. Obligation de loyauté 
    
La présente Charte contient une obligation de loyauté au travers de laquelle les Parties s'engagent à : 

* Mettre en œuvre une Démarche réellement ouverte, ce qui interdit toute volonté de manipulation de l'opinion publique ou d'instrumentalisation des contributions ;

* Agir pour leur compte ou pour celui d'un tiers identifié ;

* Ne pas chercher à fausser la Démarche dans sa représentativité (*Par ex. : création de comptes fictifs, influence d'opinion, etc.)*

* Respecter des principes de bienveillance et d'inclusion. 

### 3. Contexte de la Démarche
Dans un impératif de sincérité, il est important de : 
* Situer chaque Démarche dans un contexte précis *(Par ex. : état d'avancement et marge de modification du projet ou de la décision, type de contribution attendue entre avis, décision ou évaluation)*; 
* Partager clairement les objectifs attendus ; 
* Préciser les moyens mis en oeuvre aussi bien pour le déploiement de la Démarche que la réalisation effective des décisions prises dans ce cadre. 


### 4. Respect des réglementations applicables

Par principe, les Parties s'engagent à respecter les textes en vigueur, référencés en Annexe X. 

## [Section 1] COLLABORATION

La démocratie se construisant par le rassemblement des énergies, toute Démarche participative doit être pensée et réalisée de manière collaborative. Les Parties prennent les mesures nécessaires pour que la gestion de la Démarche favorise l'implication de toutes les personnes concernées pendant toute la durée de la Démarche.

### 5. Transversalité

Le Responsable s’assure que la Démarche participative entreprise n’est pas déjà existante. Il associe dans la mesure du possible, toute personne ou entité qui a mené ou qui a exprimé le souhait de mener une Démarche participative sur un objet similaire. Il veille à associer les Parties directement intéressées. 

### 6. Gestion de la démarche

La conception et la gestion de la Démarche sont contributives et inclusives. Les Parties participent notamment à :
* la définition des grandes orientations de la Démarche ; 
* la définition de la méthodologie (participation et évaluation) ;
* le choix du / des Dispositifs numériques et de leurs paramétrages ;
* les règles de Gouvernance de la Démarche.

Pour éviter des mécanismes d'influence cachés (lobbying, instrumentalisation), la transparence sur les Parties prenantes et leurs engagements est nécessaire. Les Parties prenantes doivent ainsi impérativement :
* être identifiées de manière visible et transparente ;
* expliquer les raisons de leur implication ;
* expliciter les engagements pris dans le cadre de la Démarche ;
* mentionner tout conflit d'intérêt potentiel.

### 7. Une Gouvernance claire et partagée

* Les règles de Gouvernance sont publiques, transparentes et compréhensibles par l’ensemble des Parties prenantes.

* La Gouvernance est adaptée aux enjeux de la Démarche.

* La Gouvernance permet d’assurer à chaque Partie prenante la garantie de son implication notamment en indiquant clairement les rôles, y compris consultatifs de chacune. 

* Le pouvoir décisionaire au sein de la Démarche ne peut ni ne doit être corrélé au financement de celle-ci.

* Toutes les Parties prenantes de la Gouvernance sont libres de communiquer publiquement les positions et avis qu'elles ont tenus dans le cadre de cette Gouvernance.

## [Section 2] PARTICIPATION

Le **Responsable** s'engage à favoriser la participation de toutes celles et ceux concernés par la démarche. Des outils et méthodes (voire FAQ) dédiés sont mobilisés pour atteindre un degré de participation et d'inclusion optimal. 

Les **Participants** reconnaissent l'intérêt d'une implication continue et s'engagent, dans la mesure du possible, à répondre aux sollicitations s'inscrivant dans le cadre de la Démarche.


### 8. Accès à la Démarche participative 

Par principe, la Démarche participative doit être ouverte à toute personne concernée ou intéressée par l'objet et/ou les résultats de la Démarche. 

Exceptionnellement, dans le cas d'une Démarche cherchant à mobiliser une expertise spécifique sur un objet, le Responsable peut choisir de cibler et limiter le nombre de Participants. Il doit alors s'appuyer sur des critères objectifs et argumentés, et rendre publique cette décision une fois validée par la Gouvernance de la Démarche.

### 9. Inclusion 

Le Responsable veille à mettre en place une Démarche inclusive afin de favoriser la participation de tous, en mettant en oeuvre tous les moyens disponibles à cet effet *(Par ex. : documentation, animation, accessibilité etc.)*. 

Le Responsable reconnaît à tous les Participants la compétence de fournir des avis et propositions, et s'engage donc à prendre en compte la parole de tous et toutes et à favoriser l'expression et la prise de confiance dans la légitimité de chacun à s'exprimer.

Une articulation entre Dispositifs numériques de participation et temps de participation en présence est organisée dans le cadre de la Démarche, notamment dans l'objectif d'inclure les publics éloignés des usages numériques.

Les règles de participation - via les différentes fonctionnalités participatives (votes, commentaires, contributions écrites, annotations, boîte à idées, classement des propositions, etc.) proposées - sont présentées clairement aux participants. 

### 10. Accessibilité

Un degré suffisant d'accessibilité est garanti dans le cadre de la Démarche. Le Dispositif numérique mobilisé dans le cadre de la Démarche fait l'objet de tests techniques automatisés en matière d'accessibilité et doit obtenir des résultats satisfaisants *(voir Annexe Y - Renvoi vers test standard + renvoi RGAA)*. 

Dans la mesure du possible, chaque Dispositif numérique de participation est testé spécifiquement, les choix graphiques et de customisation effectués pour chaque cas spécifique étant souvent déterminants. 

### 11. Gestion des contributions
Le Responsable doit pouvoir justifier de la méthodologie employée pour traiter les contributions. 

Afin de favoriser la confiance dans la Démarche, le Responsable doit assurer aux Participants des garanties : 

* Le Responsable veille à ce que la visibilité donnée aux contributions soit équitable. 

* Chaque contribution fait l'objet d'un traitement. 

* Les contributions sont modérées *a posteriori*. Sont supprimées ou masquées les contributions contenant des propos : 
    * diffamatoires ou insultants, qui inciteraient à la discrimination, à la haine, ou à la violence contre une personne ou un groupe de personnes en raison de leur lieu d’origine, de leur ethnie ou absence d’ethnie, de la nationalité ou d’une religion spécifique, 
    * incitant à la discrimination, la haine, la violence contre les personnes en raison de leur sexe, orientation sexuelle, leur identité de genre ou leur handicap. 

* Les contributions modérées sont déplacées dans une section dédiée visible par tous. 

* Le Responsable met en œuvre les mécanismes nécessaires à la gestion des influences directes ou indirectes. 
    * Les Participants représentant une organisation (publique ou privée) doivent être clairement identifiés comme tels. Leurs contributions doivent être facilement identifiables *(Par ex. : en dressant une liste des groupes d'intérêts présents, voire en proposant une identification visuelle permettant de singulariser les lobbys, les représentants politiques, etc.)*. 
    * Le Responsable veille à repérer (si nécessaire en ayant recours à un prestataire ou partenaire extérieur) les contributions de groupe et tentatives de plaidoyer déguisées (*Par ex. : contributions en grand nombre ayant le même texte)*. Le Responsable définit de manière transparente comment ce type de contributions seront prises en compte.


## [Section 3] TRANSPARENCE  


Toutes les informations relatives au Dispositif doivent être claires, complètes, précises et facilement accessibles. Toutes les composantes de la Démarche ainsi que les Dispositifs numériques sont clairement identifiés, accessibles et explicités. Les Parties prenantes de la Démarche sont clairement identifiables.

### 12. Logiciel libre et open source

Les logiciels mobilisés dans le cadre du Dispositif numérique de la Démarche doivent être libres et respecter les quatre libertés de l’utilisateur : 
* la liberté d’exécuter le programme
* la liberté de distribuer le programme
* la liberté d’étudier et de modifier le programme 
* la liberté de redistribuer des copies du programme modifié. Le code source est mis à disposition sous les termes d'une licence open source et / ou libre spécifiée. 

Le code doit être de qualité et, dans la mesure du possible, annoté. Les programmes doivent être documentés afin de faciliter l’étude, la modification et la réutilisation du code, ou d'une partie du code, par tout acteur intéressé.


### 13. Explicabilité des algorithmes 

Tout algorithme ou règle de mise en œuvre pour les décisions individuelles doit être identifié au sein du Dispositif numérique et faire l’objet d’une documentation claire à visée explicative. 

Les Participants doivent être avertis de tout traitement algorithmique réalisé au sein du Dispositif numérique ; de même, toute décision individuelle prise sur le fondement d'un tel traitement comporte une mention explicite en informant l'intéressé.

### 14. Données personnelles

Le Dispositif respecte le Règlement 2016/679 portant sur la protection, le traitement et le libre circulation des données à caractère personnel des personnes physiques.

Le Responsable identifie précisément les données à caractère personnel collectées et les finalités de cette collecte aux Participants, qui sont libres d'y consentir.

Par principe, aucune donnée à caractère personnel qui ne serait pas nécessaire à l'objet de la Démarche n'est collectée. Toutes les données personnelles collectées ne peuvent être exploitées que dans le cadre des finalités pour lesquelles elles auraient été collectées, sauf consentement complémentaire exprès des personnes concernées. Le droit d'opposition, de rétractation et de réclamation  des personnes physiques concernées est facilité.

Chaque Participant dispose d'un droit d'accès, de rectification, de modification et de suppression des données qui le concerne. Ainsi, les données personnelles sont facilement disponibles, interopérables et téléchargeables dans un ou plusieurs formats ouverts par le Participant concerné. 


*Transparence du traitement statistique (traqueurs?) [voir avec OSP]
--> Organisme de référence : la CNIL  [Il faudrait une déclaration CNIL générique : à voir][La CNIL est en train de préparer un rapport sur les données personnelles dans la "civic tech", qui va sortir à la fin de l'année - pour l'instant on ne sait pas ce qu'il y aura dedans]*

> [name=Ophélie] Il pourrait y avoir ici une partie (15 ou 14 bis ?) consacrée au traitement statistique lié au suivi des parcours utilisateurs :
>  Dans le cas d'une utilisation d'un outil de suivi du parcours utilisateur, parcourant la donnée quantitative (nombre de clics d'un utilisateur, nombre de visites,...) ou qualitative (visibilité du parcours d'un utilisateur sur la plateforme ou le site), un impératif de transparence devrait garantir l'information de l'utilisateur/contributeur de l'usage du ou des outils de tracking et lui permettre de refuser l'enregistrement de ces données. En effet, celles-ci participent à la fois à la production d'une valeur et à la définition du profil utilisateur (habitudes, parcours préférés, difficultés d'usage, ...).
>  Ces données, une fois analysées, apportent des informations qui participent à l'amélioration fonctionnelle d'une plateforme, mais peuvent également être utilisées comme support d'analyse cognitive. 

### 15. Identification des Parties prenantes

Le niveau d'identification *(Par ex. : nom, prénom, profession, appartenance politique ou syndicale...)* des personnes physiques doit être proportionné aux finalités de la Démarche. 

### 16. Open FAIR data par défaut

A l’exception des données personnelles, toutes les données, reçues, transformées ou produites au sein du Dispositif sont soumises aux régimes d’ouverture, de partage des données publiques et du FAIR data par défaut. Ces données sont intègres et mises à disposition des Parties prenantes dans un ou plusieurs formats ouverts, sous les termes d'une licence ouverte spécifiée (voir FAQ).


### 17. Conservations des contributions et traitements ultérieurs 

Les modalités de conservation des contributions et des traitements ultérieurs sont rendues publiques. 
 

## [Section 4] ENGAGEMENT 

Le Responsable de la Démarche s'engage à définir de manière claire et transparente ses engagements et à les respecter. 

### 18. Sécurité 
Le Responsable et les Participants sont conscients de l’évolution constante des technologies et des risques de sécurité inhérentes à ces dernières. Le Responsable s’assure de la sécurité des Dispositifs numériques déployés dans le cadre de la Démarche (voir FAQ). 

En cas de fuites de données, de bugs ou d’attaques des systèmes informatiques, le Responsable est tenu d’en informer, en toute transparence, l’ensemble des Participants.

### 19. Hébergement

Les hébergeurs des plateformes et des données doivent être localisés sur le territoire national du Responsable du DPN.

### 20. Synthèse de la Démarche & suivi des réalisations
Une ou plusieurs synthèses sont réalisées par le Responsable et partagées auprès des Participants. La production d'autres synthèses par des acteurs tiers est encouragée. Ces synthèses sont référencées et accessibles publiquement. 

Le Responsable précise les engagements pris dans le cadre de la Démarche pour toutes les contributions récoltées. 

Toutes les actions mises en œuvre à la suite de la Démarche sont publicisées sur les différents Dispositifs numériques ayant servi à la participation. Dans la mesure du possible, les Participants sont avertis personnellement. 

### 21. Évaluation de la démarche 

Les critères d'évaluation de la Démarche sont définis en amont de celle-ci.

Lorsque possible, un ou plusieurs évaluateurs indépendants sont nommés par le Responsable afin de produire une évaluation indépendante de la Démarche. 

Par ailleurs, tout Participant doit être en capacité de réaliser une telle évaluation à partir des données fournies par le Responsable. 

## ANNEXE 1 : TEXTES DE RÉFÉRENCE 

* En matière de participation, il n'existe de textes que dans le cas d'une concertation réglementaire. 

* Référentiel Général d'Accessibilité pour les Administrations (RGAA) Version 3 2017 : https://references.modernisation.gouv.fr/rgaa-accessibilite/

* LOI n° 2016-1321 du 7 octobre 2016 pour une République numérique : https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=D36077B77783A8D14CCA8430CCA55C45.tplgfr22s_2?cidTexte=JORFTEXT000033202746&categorieLien=id

* Référentiel Général d'Interopérabilité : https://references.modernisation.gouv.fr/interoperabilite

* Loi n° 2005-102 du 11 février 2005 pour l'égalité des droits et des chances, la participation et la citoyenneté des personnes handicapées
