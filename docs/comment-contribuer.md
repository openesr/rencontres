# Comment contribuer ? 

> Code de conduite en cours de développement. Il s'agit d'une première base à discuter en fonction de la suite donnée au projet.

Si ce projet vous intéresse, n'hésitez pas à contribuer en : 
- proposant des thématiques, des rencontres
- partager des ressources et du contenu 
- etc.

Vous pouvez contribuer directement via ce dépôt [framagit](https://framagit.org/openesr/rencontres) en ouvrant des issues/ en envoyant des merge request, ou si vous préférez, envoyez-nous un courriel team@inno3.fr ou contact@hackyourphd.org

Nous proposons un code de conduite standard explicité ci-dessous. En soumettant des contributions, vous acceptez également de les placer sous la licence CC-BY-SA 4.0 sous laquelle le projet est publié et attestez que vous possédez les droits appropriés pour le faire. 

# Code de conduite pour les personnes souhaitant contribuer

## Engagement

Dans le but de favoriser un environnement ouvert et accueillant, nous, en tant que personnes contribuants et/ou responsables de ce projet, nous nous engageons à ce que le projet et la communauté y participant soit un espace libre de tout harcèlement conscient et de veiller à une réflexion collective sur les biais cognitifs amenant à des comportements inapropriés. Le harcèlement s’entend de tout comportement inacceptable ou déplacé, dont il est vraisemblable de penser qu’il peut offenser ou humilier une autre personne ou qu’il peut être perçu comme tel, sous quelque forme que ce soit, fondé sur le genre, l’identité de genre et son expression, l’orientation sexuelle, l’appartenance ethnique, l’origine nationale, l’affiliation politique, l’âge, la religion, le niveau d'expérience, d'éducation, de statut socio-économique ou tout autre motif. 

## Principes pour maintenir le suivi du projet dans les meilleurs conditions possibles

Voici quelques exemples de comportements permettant d'enrichir collectivement le projet : 

* Utiliser un langage accueillant et inclusif
* Respecter les différents points de vue et expériences
* Accepter les critiques constructives
* Remettre en perspective les propos par rapport à ce qui pourrait être le plus bénéfique pour le projet et la communauté 
* Faire preuve d'empathie envers les autres personnes participantes

Voici quelques exemples de comportements mettant en jeu le projet et sa continuité :

* L'utilisation d'un langage ou d'une imagerie sexualisée et d'une attention sexuelle non souhaitée ou avance
* Commentaires insultants, attaques personnelles et/ou politiques
* Harcèlement public ou privé
* Publication d'informations privées d'autrui, telles que l'adresse (mail ou postal), le téléphone sans autorisation explicite
* Autre comportement qui pourrait raisonnablement être considéré comme inapproprié dans un
  cadre professionnel/associatif/coopératif

## Responsabilités 

Les personnes contribuant au projet sont chargées d'aider au respect des éléments présentés dans ce document. Elles sont censées prendre des mesures correctives appropriées et équitables dans le cas notamment de comportement nuisant au maintien du projet. Dans ce cadre, elles ont le droit et la responsabilité de supprimer, modifier ou rejeter les commentaires, les engagements, le code, les modifications du wiki, les questions et autres contributions
qui ne sont pas alignés sur le présent code de conduite, ou d'interdire temporairement ou de façon permanente toute personne dont les contributions sont jugées inappropriées ou nuisibles.


## Portée

Ce "code de conduite" s'applique aussi bien dans les espaces du projet que dans les espaces publics physiques ou numériques lorsqu'une personne représente le projet ou sa communauté. Cela peut par exemple consister à l'envoi d'un e-mail officiel sur le projet, l'affichage via un compte officiel sur les médias sociaux ou lors d'évènement public (physique ou virtuel). L'image du projet peut être discuté et clarifié avec l'aide des personnes responsables/veillant à la médiation. 

## Mise en œuvre

Les cas de comportement abusif, de harcèlement ou d'autres actions jugées comme rédhibitoires pour la vie du projet peuvent être transmis à l'équipe du projet à l'adresse team@inno3.fr ou contact@hackyourphd.org. Ces éléments seront examinées et étudiées et donneront lieu à une réponse qui est jugée nécessaire et appropriée aux circonstances. La confidentialité à l'égard de la personnes rapportant un incident sera maintenue. De plus amples détails sur les politiques d'application spécifiques peuvent être fournies si besoin.

Les personnes responsables du projet qui ne suivent pas ou n'appliquent pas le "code de conduite" en bonne et due forme, peuvent faire l'objet de répercussion 
temporaires ou permanentes. 

## Attribution

Ce code de conduite est adapté du [Pacte des contributeurs](https://www.contributor-covenant.org), version 1.4, disponible [ici](https://www.contributor-covenant.org/version/1/4/code-of-conduct.html), et adapté et traduit de la charte ethique d'Open Law, disponible [ici](https://framagit.org/OpenLawFr/charte-ethique)

Pour des réponses aux questions les plus courantes sur ce code de conduite, voir leur [FAQ](https://www.contributor-covenant.org/faq)

