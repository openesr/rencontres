![rencontre OpenESR](./img/ESR-fond-blanc.png)

Temps d’échange initié par [inno³](https://inno3.fr/) en collaboration avec l’association [HackYourResearch](https://hyr.science/), les rencontres #OpenESR donnent la parole à des chercheur.e.s et expert.e.s spécialisé.e.s sur les problématiques des communs et de l'*open* tout autant qu'à des personnes qui s'inscrivent dans ces démarches sans forcément s'en revendiquer. L'objectif est de porter un regard réflexif sur ce que ces projets/développements de concepts et de modèles peuvent inspirer en termes de transformation organisationnelle au sein de l'Enseignement Supérieur et de la Recherche (ESR) et des relations sciences/sociétés.

## Actualités

**Les rencontres OpenESR reviennent en 2021.**

> En attendant, (re)plongez vous dans le webinaire du 15 septembre 2020 avec Nicole Alix, Geneviève Fontaine et Guillaume Compain sur les projets de recherche-action au sein de La Coop des Communs. [compte-rendu](https://inno3.fr/actualite/synthese-de-la-rencontre-open-esr-avec-la-coop-des-communs-diversite-des-formes-de), [vidéo du webinaire](https://inno3.fr/actualite/replay-du-webinaire-diversite-des-formes-de-recherche-action) et [interview](https://archive.org/details/itw_genevieve_fontaine) pour HackYourResearch. #science_ouverte #communs #recherche_action

![rencontre OpenESR](./img/2020-09_Coop_communs.png)


## Newsletter

 Tenez-vous au courant des prochaines rencontres en vous inscrivant à la [newsletter](https://nouvelles.inno3.cricket/subscription/pPkh0nSG-)
