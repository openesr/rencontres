![rencontre OpenESR](./img/ESR-fond-blanc-horizontal.png)

Temps d’échange initié par [inno³](https://inno3.fr/) en collaboration avec l’association [HackYourResearch](https://hyr.science/), les rencontres #OpenESR donnent la parole à des chercheur.e.s et expert.e.s spécialisé.e.s sur les problématiques des communs et de l'*open* tout autant qu'à des personnes qui s'inscrivent dans ces démarches sans forcément s'en revendiquer. L'objectif est de porter un regard réflexif sur ce que ces projets/développements de concepts et de modèles peuvent inspirer en termes de transformation organisationnelle au sein de l'Enseignement Supérieur et de la Recherche (ESR) et des relations sciences/sociétés.


## Rencontres passées

- [15 septembre 2020] Nicole Alix, Geneviève Fontaine, Guillaume Compain - La Coop des Communs : diversité de forme de recherche-action : [compte-rendu](https://inno3.fr/actualite/synthese-de-la-rencontre-open-esr-avec-la-coop-des-communs-diversite-des-formes-de), [vidéo du webinaire](https://inno3.fr/actualite/replay-du-webinaire-diversite-des-formes-de-recherche-action) et [interview](https://archive.org/details/itw_genevieve_fontaine) pour HackYourResearch. #science_ouverte #communs #recherche_action #gouvernance

- [29 juin 2020] Florence Piron - Science ouverte et communs : [compte-rendu](https://inno3.fr/actualite/replay-du-webinaire-science-ouverte-et-communs), [vidéo du webinaire](https://media.inno3.cricket/videos/watch/b8aba757-24ff-4019-9b74-184d07324333) et [interview](https://archive.org/details/2020-06-29-florence-piron) pour HackYourResearch. #science_ouverte #communs #edition #communautés #Suds


- [26 mai 2020] Christophe Masutti - Surveillance & numérique : quelles implications pour et par la recherche ? : [compte-rendu](https://md.inno3.fr/gRmqcskHQ8W43LXl5WPblA#), [vidéo du webinaire](https://libre.video/videos/watch/a18a5cee-3513-4edb-bd9b-43ab9fd5b811) et [interview](https://archive.org/details/open-esr-christophe-masutti) pour HackYourResearch.  #Libertés  #informatisation #société #data #privacy

- [26 février 2020] Alexandre Monnin : [compte-rendu](https://md.inno3.fr/s/BJP4RdRL8) et [interview](https://archive.org/embed/openesr-alexandre-monnin) #communs_negatifs #ESR #coopératives #anthropocène #philosophie #design




## Prochaines rencontres

Les rencontres OpenESR reviennent sous format trimestriel en 2021 ! 

- [prochainement] #cooperative  #recherche-action #documentation

- [prochainement] #communs  #afrique  #démocratie participative  #éthique des sciences

- [prochainement] #open_infrastructure  #open source software #International

- [prochainement] #données_intéretgeneral #open data

- [prochainement] #coopérativisme de plateformes  #économie collaborative

- [prochainement] #coopérative #droit  #mutualisation de services  #anti_uberisation

- [prochainement] #coopérative #expert-comptable  #codesign  #ESS

- [prochainement] #collaboration inclusive  #codesign  #innovation #coopétition

- [prochainement] #économie sociale  #banque coopérative  #monde associatif

## Contribuer aux rencontres

Si vous souhaitez participer au projet, interagir avec les intervenant.e.s, présenter vos propres travaux ou réflexions sur un de ces sujets, n'hésitez pas à lire la page ["Contribuer?"](https://openesr.frama.io/rencontres/comment-contribuer/)



Le contenu du site est en Licence CC-BY-SA 4.0, pour plus d'informations, consultez la page ["Contribuer" ?](https://openesr.frama.io/rencontres/comment-contribuer/)
